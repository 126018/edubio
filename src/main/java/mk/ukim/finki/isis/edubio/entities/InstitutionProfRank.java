package mk.ukim.finki.isis.edubio.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.tapestry5.beaneditor.NonVisual;

/*
 * TODO: REMOVE THIS ENTITY. USE ISIS INSTITUTION ENTITY INSTEAD
 * */

@Entity
@Table(schema = "edubio", name = "institutions_prof_rank")
public class InstitutionProfRank {
	private Long id;

	private String name;

	private String city;

	private String country;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@NonVisual
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}
}
