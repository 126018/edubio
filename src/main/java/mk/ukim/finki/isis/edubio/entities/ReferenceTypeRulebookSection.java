package mk.ukim.finki.isis.edubio.entities;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.apache.tapestry5.beaneditor.NonVisual;

@Entity
@Table(schema = "edubio", name = "reference_type_rulebook_section")
public class ReferenceTypeRulebookSection {
	private Long id;

	private ReferenceType referenceType;

	private RulebookSection rulebookSection;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@NonVisual
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "reference_type_id")
	public ReferenceType getReferenceType() {
		return referenceType;
	}

	public void setReferenceType(ReferenceType referenceType) {
		this.referenceType = referenceType;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "rulebook_section_id")
	public RulebookSection getRulebookSection() {
		return rulebookSection;
	}

	public void setRulebookSection(RulebookSection rulebookSection) {
		this.rulebookSection = rulebookSection;
	}
}
